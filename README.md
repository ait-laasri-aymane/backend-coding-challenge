# iMedia24 Coding challenge

## How to use the application

### Docker

1. Clone the repository
2. Run `docker-compose up -d`
3. Wait until the containers are up and running
4. Open `http://localhost:8080` in your browser
5. Refer to the [API documentation](http://localhost:8080/swagger-ui.html) for the available endpoints

### IDE

1. Clone the repository
2. Open it in your favorite IDE (IntelliJ IDEA is recommended)
3. Wait until the build is finished
3. Run the application
4. Wait until the application is up and running
4. Open `http://localhost:8080` in your browser
5. Refer to the [API documentation](http://localhost:8080/swagger-ui.html) for the available endpoints

### Terminal

1. Clone the repository
2. Run `./gradlew dependencies build`
3. Wait until the build is finished
3. Run `./gradlew bootRun`
4. Wait until the application is up and running
4. Open `http://localhost:8080` in your browser
5. Refer to the [API documentation](http://localhost:8080/swagger-ui.html) for the available endpoints

## How to run the tests

### Docker

1. Clone the repository
2. Run `docker-compose up -d`
3. Wait until the containers are up and running
4. Run `docker exec -it <Container-Name> ./gradlew clean test`
5. Wait until the tests are finished

### IDE

1. Clone the repository
2. Open it in your favorite IDE (IntelliJ IDEA is recommended)
3. Wait until the build is finished
4. Run the tests
5. Wait until the tests are finished
6. Check the test report in `build/reports/tests/test/index.html`

### Terminal

1. Clone the repository
2. Run `./gradlew dependencies build`
3. Wait until the build is finished
4. Run `./gradlew test`
5. Wait until the tests are finished
6. Check the test report in `build/reports/tests/test/index.html`
