INSERT INTO products (sku, name, description, price, created_at, updated_at, stock)
VALUES ('1', 'Product 1', 'Description for Product 1', 19.99, NOW(), NOW(), 0),
       ('2', 'Product 2', 'Description for Product 2', 29.99, NOW(), NOW(), 10),
       ('3', 'Product 3', 'Description for Product 3', 39.99, NOW(), NOW(), 20),
       ('4', 'Product 4', 'Description for Product 4', 49.99, NOW(), NOW(), 30),
       ('5', 'Product 5', 'Description for Product 5', 59.99, NOW(), NOW(), 40);
