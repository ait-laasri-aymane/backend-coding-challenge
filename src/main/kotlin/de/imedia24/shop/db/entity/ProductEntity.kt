package de.imedia24.shop.db.entity

import org.hibernate.annotations.UpdateTimestamp
import java.math.BigDecimal
import java.time.ZonedDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "products")
open class ProductEntity(
        @Id
        @Column(name = "sku", nullable = false)
        open val sku: String,

        @Column(name = "name", nullable = false)
        open val name: String,

        @Column(name = "description")
        open val description: String? = null,

        @Column(name = "price", nullable = false)
        open val price: BigDecimal,

        @UpdateTimestamp
        @Column(name = "created_at", nullable = false)
        open val createdAt: ZonedDateTime,

        @UpdateTimestamp
        @Column(name = "updated_at", nullable = false)
        open val updatedAt: ZonedDateTime,

        @Column(name = "stock", nullable = false)
        open val stock: Int
)