package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductRequest.Companion.toProductEntity
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import org.springframework.stereotype.Service
import java.time.ZonedDateTime

@Service
class ProductService(private val productRepository: ProductRepository) {

    fun findProductBySku(sku: String): ProductEntity? {
        return productRepository.findBySku(sku)
    }

    fun saveProduct(productRequest: ProductRequest): ProductResponse? {
        val productResponse: ProductResponse? = findProductBySku(productRequest.sku)?.toProductResponse()
        if (productResponse != null) {
            return null
        }
        val productEntity: ProductEntity = productRequest.toProductEntity()
        val savedProductEntity: ProductEntity = productRepository.save(productEntity)
        return savedProductEntity.toProductResponse()
    }

    fun updateProduct(productRequest: ProductRequest): ProductResponse? {
        val existingProduct: ProductEntity = findProductBySku(productRequest.sku)
                ?: return null
        val updatedProductEntity = ProductEntity(
                sku = existingProduct.sku,
                name = productRequest.name ?: existingProduct.name,
                description = productRequest.description ?: existingProduct.description,
                price = productRequest.price ?: existingProduct.price,
                stock = productRequest.stock ?: existingProduct.stock,
                createdAt = existingProduct.createdAt,
                updatedAt = ZonedDateTime.now()
        )
        val savedProductEntity: ProductEntity = productRepository.save(updatedProductEntity)
        return savedProductEntity.toProductResponse()
    }

    fun countProducts(): Long {
        return productRepository.count()
    }
}
