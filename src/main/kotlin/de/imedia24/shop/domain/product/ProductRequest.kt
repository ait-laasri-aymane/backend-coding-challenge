package de.imedia24.shop.domain.product

import de.imedia24.shop.db.entity.ProductEntity
import java.math.BigDecimal
import java.time.ZonedDateTime

data class ProductRequest(
        var sku: String,
        var name: String?,
        var description: String?,
        var price: BigDecimal?,
        var stock: Int?,
) {
    companion object {
        fun ProductRequest.toProductEntity() = ProductEntity(
                sku = sku,
                name = name ?: "",
                description = description ?: "",
                price = price ?: BigDecimal.ZERO,
                stock = stock ?: 0,
                createdAt = ZonedDateTime.now(),
                updatedAt = ZonedDateTime.now()
        )
    }
}
