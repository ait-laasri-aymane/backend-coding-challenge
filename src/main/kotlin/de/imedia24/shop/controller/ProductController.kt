package de.imedia24.shop.controller

import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import de.imedia24.shop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("api/v1")
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @GetMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(
            @PathVariable("sku") sku: String
    ): ResponseEntity<Any> {
        logger.info("Request for product $sku")

        val product: ProductResponse? = productService.findProductBySku(sku)?.toProductResponse()
        return if (product == null) {
            logger.info("Product not found")
            ResponseEntity.status(HttpStatus.NOT_FOUND).body("Product not found")
        } else {
            logger.info("Product found $product")
            ResponseEntity.ok(product)
        }
    }

    @GetMapping("/products", produces = ["application/json;charset=utf-8"])
    fun findProductsBySkus(
            @RequestParam("skus") skuList: List<String>
    ): ResponseEntity<Any> {
        logger.info("Request for products $skuList")

        val products: List<ProductResponse> = skuList.mapNotNull { productService.findProductBySku(it)?.toProductResponse() }
        return if (products.isEmpty()) {
            logger.info("Products not found")
            ResponseEntity.status(HttpStatus.NOT_FOUND).body("Products not found")
        } else {
            logger.info("Products found $products")
            ResponseEntity.ok(products)
        }
    }

    @PostMapping("/products", produces = ["application/json;charset=utf-8"])
    fun saveProduct(
            @RequestBody productRequest: ProductRequest
    ): ResponseEntity<Any> {
        logger.info("Request to save product $productRequest")
        val product = productService.saveProduct(productRequest)
        return if (product == null) {
            logger.error("Product with sku ${productRequest.sku} already exists")
            ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Product with sku ${productRequest.sku} already exists")
        } else {
            logger.info("Product saved $product")
            ResponseEntity.ok(product)
        }
    }

    @PatchMapping("/products", produces = ["application/json;charset=utf-8"])
    fun updateProduct(
            @RequestBody productRequest: ProductRequest
    ): ResponseEntity<Any> {
        logger.info("Request to update product $productRequest")
        val product = productService.updateProduct(productRequest)
        return if (product == null) {
            logger.error("Product with sku ${productRequest.sku} does not exist")
            ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Product with sku ${productRequest.sku} does not exist")
        } else {
            logger.info("Product updated $product")
            ResponseEntity.ok(product)
        }
    }

    @GetMapping("/products/count", produces = ["application/json;charset=utf-8"])
    fun countProducts(): ResponseEntity<Any> {
        logger.info("Request to count products")
        val count = productService.countProducts()
        return ResponseEntity.ok(count)
    }
}
