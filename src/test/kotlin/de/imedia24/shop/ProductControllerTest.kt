package de.imedia24.shop

import de.imedia24.shop.controller.ProductController
import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductRequest.Companion.toProductEntity
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import de.imedia24.shop.service.ProductService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.springframework.http.HttpStatus
import java.math.BigDecimal
import java.time.ZonedDateTime

class ProductControllerTest {

    @Mock
    private lateinit var productService: ProductService

    private lateinit var productController: ProductController

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        productController = ProductController(productService)
    }

    @Test
    fun testFindProductsBySku_ProductFound() {
        val sku = "123"
        val product = ProductEntity(
                sku = "123",
                name = "Test Product",
                description = "Test Description",
                price = BigDecimal(10),
                stock = 10,
                createdAt = ZonedDateTime.now(),
                updatedAt = ZonedDateTime.now()
        )

        Mockito.`when`(productService.findProductBySku(sku)).thenReturn(product)

        val response = productController.findProductsBySku(sku)

        assert(response.statusCode == HttpStatus.OK)
        assert(response.body == product.toProductResponse())
    }

    @Test
    fun testFindProductsBySku_ProductNotFound() {
        val sku = "123"

        Mockito.`when`(productService.findProductBySku(sku)).thenReturn(null)

        val response = productController.findProductsBySku(sku)

        assert(response.statusCode == HttpStatus.NOT_FOUND)
        assert(response.body == null)
    }

    @Test
    fun testFindProductsBySkus_ProductsFound() {
        val skus: List<String> = listOf("123", "456", "789")

        val product1 = ProductEntity(
                sku = "123",
                name = "Test Product 1",
                description = "Test Description 1",
                price = BigDecimal(10),
                stock = 10,
                createdAt = ZonedDateTime.now(),
                updatedAt = ZonedDateTime.now()
        )
        val product2 = ProductEntity(
                sku = "456",
                name = "Test Product 2",
                description = "Test Description 2",
                price = BigDecimal(20),
                stock = 20,
                createdAt = ZonedDateTime.now(),
                updatedAt = ZonedDateTime.now()
        )

        Mockito.`when`(productService.findProductBySku("123")).thenReturn(product1)
        Mockito.`when`(productService.findProductBySku("456")).thenReturn(product2)
        Mockito.`when`(productService.findProductBySku("789")).thenReturn(null)

        val response = productController.findProductsBySkus(skus)

        assert(response.statusCode == HttpStatus.OK)
        assert(response.body == listOf(product1.toProductResponse(), product2.toProductResponse()))
    }

    @Test
    fun testFindProductsBySkus_ProductsNotFound() {
        val skus: List<String> = listOf("123", "456", "789")

        Mockito.`when`(productService.findProductBySku("123")).thenReturn(null)
        Mockito.`when`(productService.findProductBySku("456")).thenReturn(null)
        Mockito.`when`(productService.findProductBySku("789")).thenReturn(null)

        val response = productController.findProductsBySkus(skus)

        assert(response.statusCode == HttpStatus.NOT_FOUND)
        assert(response.body == null)
    }

    @Test
    fun testCreateProduct() {
        val productRequest = ProductRequest(
                sku = "123",
                name = "Test Product",
                description = "Test Description",
                price = BigDecimal(10),
                stock = 10
        )
        val product = productRequest.toProductEntity().toProductResponse()

        Mockito.`when`(productService.saveProduct(productRequest)).thenReturn(product)

        val response = productController.saveProduct(productRequest)

        assert(response.statusCode == HttpStatus.OK)
        assert(response.body == product)
    }

    @Test
    fun testCreateProduct_FAIL() {
        val productRequest = ProductRequest(
                sku = "123",
                name = "Test Product",
                description = "Test Description",
                price = BigDecimal(10),
                stock = 10
        )

        Mockito.`when`(productService.saveProduct(productRequest)).thenReturn(null)

        val response = productController.saveProduct(productRequest)

        assert(response.statusCode == HttpStatus.BAD_REQUEST)
        assert(response.body == null)
    }

    @Test
    fun testUpdateProduct() {
        val productRequest = ProductRequest(
                sku = "123",
                name = "Test Product MODIFIED",
                description = null,
                price = null,
                stock = 10
        )

        val productToUpdate = ProductEntity(
                sku = "123",
                name = "Test Product",
                description = "Test Description 2",
                price = BigDecimal(20),
                stock = 20,
                createdAt = ZonedDateTime.now(),
                updatedAt = ZonedDateTime.now()
        )

        val updatedProduct = ProductResponse(
                sku = "123",
                name = "Test Product MODIFIED",
                description = "Test Description 2",
                price = BigDecimal(20),
                stock = 10
        )


        Mockito.`when`(productService.findProductBySku(productRequest.sku)).thenReturn(productToUpdate)

        Mockito.`when`(productService.updateProduct(productRequest)).thenReturn(updatedProduct)

        val response = productController.updateProduct(productRequest)

        assert(response.statusCode == HttpStatus.OK)
        assert(response.body == updatedProduct)
    }

    @Test
    fun testUpdateProduct_FAIL() {
        val productRequest = ProductRequest(
                sku = "123",
                name = "Test Product MODIFIED",
                description = null,
                price = null,
                stock = 10
        )

        Mockito.`when`(productService.updateProduct(productRequest)).thenReturn(null)

        val response = productController.updateProduct(productRequest)

        assert(response.statusCode == HttpStatus.BAD_REQUEST)
        assert(response.body == null)
    }
}
