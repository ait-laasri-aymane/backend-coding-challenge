FROM openjdk:8-jdk-alpine as build

WORKDIR /app

COPY gradlew .
COPY gradle gradle

COPY build.gradle.kts .
COPY settings.gradle.kts .

COPY src src

RUN ls -l

RUN ./gradlew dependencies build

EXPOSE 8080

CMD ["java", "-jar", "build/libs/shop-0.0.1-SNAPSHOT.jar"]
